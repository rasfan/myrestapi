process.env.NODE_CONFIG_DIR = __dirname + '/config';

const config = require('config');
const express = require('express');
const _ = require('lodash');

const { User } = require('./model/user');
const { authenticate } = require('./middleware/authenticate');

console.log(
    `*** ${String(config.get('Level')).toUpperCase()} *** on port ${config.get(
        'PORT'
    )} `
);

const app = express();
app.use(express.json());

//register
app.post('/api/register', (req, res) => {
    const body = _.pick(req.body, ['fullname', 'email', 'password']);

    let user = new User(body);
    user.save().then(
        user => {
            res.status(200).send(user);
        },
        err => {
            res.status(400).json({
                Error: `Something went wrong. ${err}`
            });
        }
    );
});

//login
app.post('/api/login', async (req, res) => {
    try {
        const body = _.pick(req.body, ['email', 'password']);

        let user = await User.findByCredentials(body.email, body.password);
        let token = await user.generateAuthToken();
        res.header('x-auth', token)
            .status(200)
            .send(token);
    } catch (e) {
        res.status(401).json({
            Error: `Something went wrong. ${e}`
        });
    }
});

//logout
app.delete('/api/logout', authenticate, async (req, res) => {
    try {
        await req.user.removeToken(req.token);
        res.status(200).json({
            Message: 'Logout successfull.'
        });
    } catch (e) {
        res.status(400).json({
            Error: `Something went wrong. ${e}`
        });
    }
});

//add a payment
app.post('/api/payment', authenticate, async (req, res) => {
    try {
        const body = _.pick(req.body, ['info', 'amount']);
        let user = await User.findOneAndUpdate(
            {
                _id: req.user._id
            },
            {
                $push: {
                    payment: {
                        info: body.info,
                        amount: body.amount
                    }
                }
            }
        );

        if (!user) {
            return res.status(404).json({
                Error: 'User not found'
            });
        }

        res.status(200).json({
            Message: 'Payment has been saved.'
        });
    } catch (e) {
        res.status(400).json({
            Error: `Something went wrong. ${e}`
        });
    }
});

//get payments list
app.get('/api/payment', authenticate, async (req, res) => {
    try {
        let user = await User.findOne({
            _id: req.user._id
        });

        if (!user) {
            return res.status(404).json({
                Error: 'User not found'
            });
        }

        res.status(200).send(user.payment);
    } catch (e) {
        res.status(400).json({
            Error: `Something went wrong. ${e}`
        });
    }
});

//delete a payment by id
app.delete('/api/payment/:id', authenticate, async (req, res) => {
    let id = req.params.id;

    try {
        let user = await User.findOneAndUpdate(
            {
                _id: req.user._id,
                'payment._id': id
            },
            {
                $pull: {
                    payment: {
                        _id: id
                    }
                }
            }
        );

        if (!user) {
            return res.status(404).json({
                Error: 'User not found'
            });
        }

        res.status(200).send(user.payment);
    } catch (e) {
        res.status(400).json({
            Error: `Something went wrong. ${e}`
        });
    }
});

//update a payment
app.patch('/api/payment', authenticate, async (req, res) => {
    let body = _.pick(req.body, ['id', 'info', 'amount']);

    try {
        let user = await User.findOneAndUpdate(
            {
                _id: req.user._id,
                'payment._id': body.id
            },
            {
                $set: {
                    'payment.$.info': body.info,
                    'payment.$.amount': body.amount
                }
            }
        );

        if (!user) {
            return res.status(404).json({
                Error: 'User not found'
            });
        }

        res.status(200).json({
            Message: 'Payment updated'
        });
    } catch (e) {
        res.status(400).json({
            Error: `Something went wrong. ${e}`
        });
    }
});

//get payments with same info
app.get('/api/payment/:id', authenticate, async (req, res) => {
    let info = req.params.id;

    try {
        let user = await User.findOne({
            _id: req.user._id
        });

        let payments = [];

        if (!user) {
            return res.status(404).json({
                Error: 'User not found'
            });
        }

        user.payment.forEach(element => {
            if (element.info === info) {
                payments.push(element);
            }
        });

        res.status(200).send(payments);
    } catch (e) {
        res.status(400).json({
            Error: `Something went wrong. ${e}`
        });
    }
});

app.listen(config.get('PORT'));
